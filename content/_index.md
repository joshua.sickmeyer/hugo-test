---
title: "SOAFEE"
date: 2021-10-18T13:44:27-05:00
draft: false
description: Scalable Open Architecture for Embedded Edge (SOAFEE) is an industry-led collaboration that is bringing the best practices of cloud-native with the requirements for functional safety, security and real-time to the automotive domain.
keywords: Cloud native, Software defined, Cloud native development, Automotive software development, Cloud native technology stack, Automotive software platform
---
{{< figure class="banner-img" src="images/ARM1828_SOAFEE_Dark_ST2.svg" >}}
## Overview
The Scalable Open Architecture for Embedded Edge (SOAFEE) project is an industry-led collaboration defined by automakers, semiconductor suppliers, open source and independent software vendors, and cloud technology leaders. The initiative intends to deliver a cloud-native architecture enhanced for mixed-criticality automotive applications with corresponding open-source reference implementations to enable commercial and non-commercial offerings. 

Building on [Project Cassini](https://www.arm.com/solutions/infrastructure/edge-computing/project-cassini) and SystemReady standards, which define standard boot and security requirements for Arm architecture, SOAFEE adds the cloud-native development and deployment framework while introducing functional safety, security, and real-time capabilities required for automotive workloads.

## Components of SOAFEE
This is a high-level view of an automotive central compute solution stack showing hardware, software, and cloud levels. At the bottom level, Arm SystemReady and PSA Certified security specifications rolled out as part of Project Cassini, can ensure system integrators and software developers have a standard firmware interface that can enable seamless secure boot and system bring-up flows, across compliant Arm-based hardware.
  
The SOAFEE architecture will seek to re-use existing open standards for the different components in the framework, and will extend those standards as necessary to meet the mixed-criticality requirements of automotive applications.

SOAFEE builds on top of these specifications and standards with a reference framework to standardize key non-differentiating middle-layers, such as the hypervisor, operating systems, container runtime and hardware abstraction layers.  

![SOAFEE-Flow](images/SOAFEE-Web-Image1.png)

An initial version of SOAFEE, called the SOAFEE R1 is available to download at the [GitLab repository today](https://gitlab.arm.com/soafee). It serves as a starting point to enable automotive DevOps using cloud-native fundamentals.  

## SOAFEE Special Interest Group
The SOAFEE Special Interest Group (SIG) aims to lead and define the cloud-native development paradigm required for a new era of efficient edge workloads. The SIG comprises of the Governing Body and Working Groups.

{{< row >}}
{{% column %}}
### SOAFEE Governing Body

The SOAFEE Governing Body is responsible for defining the strategic direction of the SOAFEE project. To achieve the project goals, the Governing Body will create working groups and define the goals and objectives for these groups. The Governing Body will validate all output from the working groups to ensure that the strategic objectives of the project are met.

The Governing Body comprises a group of industry experts to ensure that market intelligence is applied to the output of the SOAFEE project. Details of membership can be found [here](https://gitlab.arm.com/soafee/governing-body/-/blob/main/Documents/SOAFEE_SIG_Public.pdf).

**License**

The software is provided under an [MIT license](https://spdx.org/licenses/MIT.html). Contributions to this project are accepted under the same license.

Documentation is provided under the [Creative Commons Attribution 4.0 International License](https://spdx.org/licenses/CC-BY-4.0.html) and all accepted contributions must have the same license.

Contributions
Details of how to make a contribution to the SOAFEE Project can be found in the [contributing](https://gitlab.arm.com/soafee/governing-body/-/blob/main/CONTRIBUTING.md) document.

{{% /column %}}

{{% column %}}

### SOAFEE Working Groups
The working groups form the core of the ecosystem engagement of the SOAFEE project. Working groups are created under the guidance of the Governing Body with the scope, deliverables and sign-off managed by the Governing Body.

Each working group will be covered by the license and contribution agreement as defined here.

The working practices of the groups are still being defined, but content from the groups will be captured in the tickets and wiki of this project.

**Working Groups**

The list of currently active working groups is:
- Ecosystem
- Standards
- Mixed Critical Orchestrator
- Functional Safety

All information on the scope of the groups can be found by following the links above.

**License**

The software is provided under an [MIT license](https://spdx.org/licenses/MIT.html). Contributions to this project are accepted under the same license.

Documentation is provided under the [Creative Commons Attribution 4.0 International License](https://spdx.org/licenses/CC-BY-4.0.html) and all accepted contributions must have the same license.

Contributions
Details of how to make a contribution to the SOAFEE project can be found in the [contributing](https://gitlab.arm.com/soafee/working-groups/-/blob/main/CONTRIBUTING.md) document.

{{% /column %}}
{{< /row >}}

## Resources and Documentation
For more information about the SOAFEE project, explore the links below:

* [White paper: How the SOAFEE architecture brings a cloud-native approach to mixed critical automotive systems](https://armkeil.blob.core.windows.net/developer/Files/pdf/white-paper/arm-scalable-open-architecture-for-embedded-edge-soafee.pdf)
* [Blog: The software-defined vehicle needs hardware that goes the distance](https://www.arm.com/blogs/blueprint/software-defined-vehicle)
* [Blog: The cloud-native approach to the software defined car](https://community.arm.com/2021-ia-reorg-archive/developer/ip-products/system/b/embedded-blog/posts/cloud-native-approach-to-the-software-defined-car?_ga=2.84349662.63983164.1633923986-188627514.1623282169&_gac=1.188151258.1631741153.Cj0KCQjws4aKBhDPARIsAIWH0JWzFX7JhWfY12ecutW_Gaiy3HwXQ1QWT1HbMuvrAnTdtsTSdk57dzAaAq7DEALw_wcB) 
* [SOAFEE on GitLab](https://gitlab.arm.com/soafee)

## Contact
If you would like to support and contribute to the SOAFEE project, please contact Robert.day@arm.com
